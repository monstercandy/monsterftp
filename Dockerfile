FROM monstercommon

ADD lib /opt/MonsterFtp/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterFtp/lib/bin/ftp-install.sh && /opt/MonsterFtp/lib/bin/ftp-test.sh

ENTRYPOINT ["/opt/MonsterFtp/lib/bin/ftp-start.sh"]
