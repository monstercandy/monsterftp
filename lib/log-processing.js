module.exports = function(app, knex){

  const Tail = require('MonsterTail').Tail;
	const ftpLib = require("lib-ftp")(app, knex)

  const loginRegexp = /proftpd\[(\d+)\] [^ ]+ \([^\[]+\[(?:::ffff:)?([^\]]+)\]\): USER ([^: ]+):? (.+)/;
  const clamavRegexp = /proftpd\[(\d+)\] [^ ]+ \([^\[]+\[(?:::ffff:)?([^\]]+)\]\): mod_clamav\/[^:]+: Virus '([^']+)' found in '.*\/([0-9]+)-[0-9a-f]{32}\/([^']+)'/;

	var re = {}

	re.Start = function(){
       var logfile = app.config.get("proftpd_log_file")
       if(!logfile) return

	    var tail = new Tail.robust(logfile);
	    tail.on("line", function(data) {
          re.processLine(data);
	    });

	}


  re.processLine = function(data){


        // console.log("hey!", data)
/*
Successful:
2017-03-12 13:12:03,339 monstermedia proftpd[31724] 88.151.102.104 (178.48.246.208[178.48.246.208]): USER foglalas: Login successful.

Unsuffessful:
2017-03-12 13:13:29,403 monstermedia proftpd[31929] 88.151.102.104 (95.110.156.190[95.110.156.190]): USER mobilhomokteam: no such user found from 95.110.156.190 [95.110.156.190] to ::ffff:88.151.102.104:21
2017-03-12 13:14:29,394 monstermedia proftpd[32087] 88.151.102.104 (112.86.74.40[112.86.74.40]): USER root: no such user found from 112.86.74.40 [112.86.74.40] to ::ffff:88.151.102.104:22
  if($l=~/proftpd\[(\d+)\] [^ ]+ \([^\[]+\[(?:::ffff:)?([^\]]+)\]\): USER ([^: ]+):? (.+)/) {

Clamav virus event:
Apr 30 10:45:24 monstermedia proftpd[24057] monstermedia.hu (::ffff:193.106.104.151[::ffff:193.106.104.151]): mod_clamav/0.10: Virus 'JS.Trojan.Redir-7' found in '/web/w3/16523-bd4d23a63b7ad6351ca59a67edd02649/suncitylove.hu/W4RYpvj0/index.html'
elsif($l=~/proftpd\[(\d+)\] [^ ]+ \([^\[]+\[(?:::ffff:)?([^\]]+)\]\): mod_clamav\/\d+\.\d+: Virus '([^']+)' found in '\/web\/w\d+\/(\d+)-[^\/]+\/([^']+)'/) {

*/

          var match = loginRegexp.exec(data);
          if(match) {

             // [ 'proftpd[6473] 88.151.102.104 (178.48.246.208[178.48.246.208]): USER ng.monstermedia.hu: Login successful.',

             // console.log(match); // abc

             var params = {}

             params.pid = match[1];
             params.ip = match[2];
             params.username = match[3];
             params.reason = match[4];
             params.successful = (params.reason == "Login successful.");

             re.processLoginEvent(params);

             return true;
          }

          match = clamavRegexp.exec(data);
          if(match){

             // console.log(match);
             var params = {}

             params.pid = match[1];
             params.ip = match[2];
             params.virus_name = match[3];
             params.webhosting_id = match[4];
             params.filename = match[5];

             re.processClamavEvent(params);

             return true;
          }


          return false;

  }

  re.processClamavEvent = function(params){
      var ip = params.ip;
      delete params.ip;
                  app.InsertEvent(null, {
                   e_ip: ip,
                   e_event_type: "virus-detected",
                   e_other: JSON.stringify(params)
                })
  }

  re.processLoginEvent= function(params){
        return ftpLib.GetAccountsBy({fa_username: params.username})
             .then(rows=>{
                  var user_id = ""

                  if(params.successful) {
                      rows.forEach(row=>{
                        if(!user_id) user_id = row.fa_user_id

                        row.UpdateLastLogin(params.ip).catch(ex=>{
                            console.error("Unable to save last login ip:", params, ex)
                        })
                      })
                  }

                  app.InsertEvent(null, {
                   e_ip: params.ip,
                   e_event_type: "auth-credential",
                   e_username: params.username,
                   e_user_id: user_id,
                   e_other: {pid: params.pid, reason:params.reason, successful: params.successful},
                })

             })
  }

	return re

}
