// NOTE: each entity here is related to the current server, since MonsterFtp will always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('adminips', function(table) {
            table.string('ai_user_id').primary();

            table.string('ai_ip').notNullable();

            table.timestamp('ai_ts').notNullable();
        }),


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('adminips'),
    ])
	
};
