// NOTE: each entity here is related to the current server, since MonsterFtp will always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('ftpaccounts', function(table) {
            table.string('fa_totp_secret');
            table.boolean('fa_totp_active').notNullable().defaultTo(false);

        }),


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('ftpaccounts', function(table){
            table.dropColumn("fa_totp_secret");
            table.dropColumn("fa_totp_active");
        }),
    ])
	
};
