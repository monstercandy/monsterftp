// NOTE: each entity here is related to the current server, since MonsterFtp will always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.createTable('webhostings', function(table) {
            table.integer('wh_webhosting').primary();

            table.string('wh_storage').notNullable();

            table.string('wh_user_id').notNullable();

            table.decimal('wh_quota_b').notNullable().defaultTo(0);            

            table.boolean("wh_public_login").notNullable().defaultTo(true);

            table.timestamp("wh_last_admin_login_ts");
            table.string("wh_last_admin_login_ip");
            table.boolean("wh_last_admin_login_enabled").notNullable().defaultTo(false);

            table.boolean("wh_ip_acl_effective").notNullable().defaultTo(false);
            table.string("wh_ip_acl_whitelist");
			
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());

         }),

         knex.schema.createTable('tallies', function(table) {
            table.integer('t_webhosting').primary();

            table.decimal('t_tally_b').notNullable().defaultTo(0);            
            table.boolean('t_tally_updated').notNullable().defaultTo(false);            

            table.index(["t_tally_updated"]);
         }),

         knex.schema.createTable('ftpaccounts', function(table) {
            table.increments('fa_id').primary();

            table.string('fa_user_id').notNullable();

            table.integer('fa_webhosting').notNullable()
                 .references('wh_webhosting')
                 .inTable('webhostings');

            table.string('fa_username').notNullable().unique();
            table.string('fa_subdir').notNullable();

            table.string('fa_password').notNullable();
            table.string('fa_pbkdf2_hash_algo').notNullable();
            table.integer('fa_pbkdf2_output_length').notNullable();
            table.integer('fa_pbkdf2_iterations').notNullable();
            table.string('fa_salt').notNullable();

            table.timestamp('fa_last_login_ts');
            table.string('fa_last_login_ip');
				 
            table.timestamp('created_at').notNullable().defaultTo(knex.fn.now());
            table.timestamp('updated_at').notNullable().defaultTo(knex.fn.now());
        }),

         knex.schema.createTable('scoreboard', function(table) {
            table.string('s_unique').primary();

            table.string('s_ip').notNullable();

            table.string('s_username').notNullable();
            table.string('s_last_command').notNullable();

            table.timestamp('s_ts').notNullable();
        }),


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('ftpaccounts'),
        knex.schema.dropTable('sites'),
        knex.schema.dropTable('scoreboard'),
    ])
	
};
