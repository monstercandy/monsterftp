// NOTE: each entity here is related to the current server, since MonsterFtp will always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('tallies', function(table) {
            table.decimal('t_tally_sum_b').notNullable().defaultTo(0);            

        }),


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('tallies', function(table){
            table.dropColumn("t_tally_sum_b");
        }),
    ])
	
};
