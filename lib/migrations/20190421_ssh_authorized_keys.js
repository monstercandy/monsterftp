// NOTE: each entity here is related to the current server, since MonsterFtp will always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('ftpaccounts', function(table) {
            table.string('fa_ssh_authorized_key');

        }),


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('ftpaccounts', function(table){
            table.dropColumn("fa_ssh_authorized_key");
        }),
    ])
	
};
