var lib = module.exports = function(app, knex){

  const KnexLib = require("MonsterKnex")
  const MError = require("MonsterError")
  var ValidatorsLib = require("MonsterValidators")

  const moment = require('MonsterMoment');

  var Common = ValidatorsLib.array()

  var vali = ValidatorsLib.ValidateJs()

  var re = {}
  function GetWebhostingsBy(filterHash){

    var f = KnexLib.filterHash(filterHash)

    return knex("webhostings")
      .select("webhostings.*", "tallies.t_tally_b", "tallies.t_tally_sum_b", "tallies.t_tally_updated")
      .leftJoin('tallies', 'tallies.t_webhosting', 'webhostings.wh_webhosting')
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      .then(rows=>{
          rows.map(row => dbRowToObject(row))
          Common.AddCleanupSupportForArray(rows)

          return Promise.resolve(rows)
      })

  }


  re.GetSites = function() {
   return GetWebhostingsBy({})
  }

  re.DoAdjustTally = function(in_data) {
  	if(!in_data) return Promise.resolve()

    return knex.transaction(trx=>{

        var ps = []
        in_data.forEach(webhosting=>{
          var wh_web_tally_b = megaBytesToBytes(webhosting.wh_tally_web_storage_mb)
          var wh_sum_tally_b = megaBytesToBytes(webhosting.wh_tally_sum_storage_mb)
          ps.push(
           trx("tallies")
            .whereRaw("t_webhosting=?", [webhosting.wh_id])
            .update({t_tally_b: wh_web_tally_b,t_tally_sum_b: wh_sum_tally_b})
            .then(c=>{
              if((c > 0) || (wh_web_tally_b == 0)) return Promise.resolve()
              return re.insertOrUpdateWebhosting(webhosting.wh_id, wh_web_tally_b, wh_sum_tally_b, trx)
            })
          )
        })
        return Promise.all(ps)


    })
  }

  re.GetWebhostingByIdSimple = function(webhostingId){
     return GetWebhostingsBy({wh_webhosting: webhostingId})
      .then(rows=>{
         Common.EnsureSingleRowReturned(rows, "WEBHOSTING_NOT_FOUND")
         return Promise.resolve(rows[0])
      })
  }

  re.GetWebhostingById= function(webhostingId){
    return vali.async({wh_id:webhostingId},{wh_id:{presence:true,isInteger:true}})
      .then(d=>{
         return re.insertOrUpdateWebhosting(webhostingId)
      })
  	  .then(()=>{
         return re.GetWebhostingByIdSimple(webhostingId);
       })
  }

  re.InsertSiteBackup = function(data, trx){
     if(!trx) trx = knex;
     var r = KnexLib.filterHash(data, true)
     return trx.raw("REPLACE INTO webhostings ("+r.sqlFilterStr+") VALUES ("+r.sqlValuesStr+")", r.sqlFilterValues).return()
  }
  re.InsertTallyBackup = function(data, trx){
     if(!trx) trx = knex;
     var r = KnexLib.filterHash(data, true)
     return trx.raw("REPLACE INTO tallies ("+r.sqlFilterStr+") VALUES ("+r.sqlValuesStr+")", r.sqlFilterValues).return()
  }


  re.insertOrUpdateWebhosting=function(webhostingId, insert_wh_tally_web_b, insert_wh_tally_sum_b, trx) {
          var updateData
          var webhosting

          if(!trx) trx = knex

          return app.MonsterInfoWebhosting.GetInfo(webhostingId)
        .then(awebhosting=>{
          webhosting = awebhosting


          updateData = {
                 wh_user_id: webhosting.wh_user_id,
                 wh_quota_b: megaBytesToBytes(webhosting.template.t_storage_max_quota_soft_mb),
          }

          if(insert_wh_tally_web_b) return Promise.resolve(0)

            return trx("webhostings")
              .whereRaw("wh_webhosting=?", [webhosting.wh_id])
              .update(updateData)
        })
        .then(x=>{
              if(x > 0) return Promise.resolve()

              updateData.wh_webhosting = webhosting.wh_id
              updateData.wh_storage = webhosting.extras.web_path

              return trx("adminips").select("ai_ip", "ai_ts").whereRaw("ai_user_id=?", [webhosting.wh_user_id])
                .then(rows=>{
                    if(rows.length == 1) {
                       updateData.wh_last_admin_login_ip = rows[0].ai_ip
                       updateData.wh_last_admin_login_ts = rows[0].ai_ts
                    }

                   return trx
                     .insert(updateData)
                     .into("webhostings")

                })
                .then(ids=>{

                   app.InsertEvent(null, {e_user_id: webhosting.wh_user_id, e_event_type: "ftp-site-inserted", e_other:{webhosting_id: webhosting.wh_id}})

                   var updateData = {
                    t_webhosting: webhosting.wh_id,
                    t_tally_b: insert_wh_tally_web_b || megaBytesToBytes(webhosting.wh_tally_web_storage_mb),
                    t_tally_sum_b: insert_wh_tally_sum_b || megaBytesToBytes(webhosting.wh_tally_sum_storage_mb),
                   }

                   // console.log("HEEEEY", updateData, webhosting)

                   return trx
                     .insert(updateData)
                     .into("tallies")
                 })
                 .then(ids=>{
                    return Promise.resolve(ids[0])
                 })


        })
        .then(()=>{
            return Promise.resolve(webhosting)
        })


  }

  re.StoreAdminIps = function(in_data) {
     return knex.transaction(function(trx){
         var now = moment.now()

         var ps = []
         Object.keys(in_data).forEach(user_id=>{
            var ip = in_data[user_id]
            ps.push(
               trx.raw("INSERT OR REPLACE INTO adminips (ai_user_id, ai_ip, ai_ts) VALUES (?,?,?)", [user_id, ip, now])
                 .then(()=>{})
            )

            ps.push(
               trx("webhostings").update({wh_last_admin_login_ip:ip, wh_last_admin_login_ts: now}).whereRaw("wh_user_id=?", [user_id])
                 .then(()=>{})
            )

         })

         return Promise.all(ps)
     })
  }

  if(!app.cleanupLastAdminLogin) {
    app.cleanupLastAdminLogin = true
    doLastAdminLoginCleanup()
  }

  re.CleanupOldAdminIps = function(){
     var until = getLastAdminLoginValidUntil(moment(), -1)

     return knex("adminips")
        .whereRaw("ai_ts<=?", [until])
        .delete()
        .then(()=>{
             return knex("webhostings")
               .whereRaw("wh_last_admin_login_ts<=?", [until])
               .update({wh_last_admin_login_ip:"", wh_last_admin_login_ts:null})
        })
  }

  return re


  function doLastAdminLoginCleanup() {
      setTimeout(function(){

        return re.CleanupOldAdminIps()
          .then(()=>{
              doLastAdminLoginCleanup()
          })

      }, app.config.get("last_admin_login_cleanup_interval_ms"))
  }


  function getLastAdminLoginValidUntil(m, order){
        if(!order) order = 1
        m.add(order*app.config.get("wh_last_admin_login_duration_sec"), "s")
        return m.toISOString()

  }


  function dbRowToObject(webhosting) {
     var webhostingId = webhosting.wh_webhosting

     webhosting.wh_ip_acl_whitelist = Common.ForceEnumerableToList(webhosting.wh_ip_acl_whitelist)

     if((webhosting.wh_last_admin_login_ip)&&(webhosting.wh_last_admin_login_ts)) {
        var m = moment(webhosting.wh_last_admin_login_ts)
        webhosting.wh_last_admin_login_allowed_until = getLastAdminLoginValidUntil(m)
     }

  	 webhosting.Cleanup = function(){
      // this goes back to the frontend and it contains sensitive info so we rather not report it
      delete webhosting.wh_storage
  	 	return webhosting
  	 }
  	 webhosting.Delete = function(){

        return knex("ftpaccounts")
              .whereRaw("fa_webhosting=?", [webhostingId])
              .del()
              .then(()=>{
                return knex("tallies")
                .whereRaw("t_webhosting=?", [webhostingId])
                .del()
              })
              .then(()=>{
              return knex("webhostings")
                .whereRaw("wh_webhosting=?", [webhostingId])
                .del()
              })
              .then(()=>{
                return Promise.resolve()
              })
  	 }

      webhosting.AddAclIp = function(json){
         return aclVali(json).then((d)=>{
            return addAclIp(knex, d.ip)
         })
      }
      webhosting.RemoveAclIp = function(json){
         return aclVali(json).then((d)=>{
            return removeAclIp(knex, d.ip)
         })
      }
      webhosting.SetAclIps = function(json){
         return vali.async(json,{
            status:{isBooleanLazy: true, },
            reset: {isIPv4Array: true, }
          })
         .then((d)=>{

            if(Object.keys(d).length <= 0) return Promise.resolve()

               var ps = []
               if(d.reset){
                   webhosting.wh_ip_acl_whitelist = d.reset
                   ps.push(
                      storeAcls(knex)
                   )
               }

               if(typeof d.status != "undefined") {
                  ps.push(loginFilter("wh_ip_acl_effective", d.status))
               }

               return Promise.all(ps)


         })
      }

      function loginFilter(q, status) {
        var c = {}
        c[q] = status
        webhosting[q] = status

        return knex("webhostings")
            .whereRaw("wh_webhosting=?", [webhostingId])
            .update(c)
            .then(()=>{
              return Promise.resolve()
            })

      }


      webhosting.SetPublic = function(json){

         return vali.async(json,{
            status:{presence: true, isBooleanLazy: true, },
         })
         .then(d=>{

            return loginFilter("wh_public_login", d.status)

         })

      }


      webhosting.StoreAdminLogin = function(json){

         return vali.async(json,{
            status:{presence: true, isBooleanLazy: true, },
         })
         .then(d=>{

            return loginFilter("wh_last_admin_login_enabled", d.status)

         })

      }


  	 return webhosting

     function aclVali(json) {
        return vali.async(json, {
          ip: {presence:true, isValidIPv4:true},
        })
     }

     function addAclIp(knex, ip){
        var index = webhosting.wh_ip_acl_whitelist.indexOf(ip);
        if(index >= 0) return Promise.resolve()
        webhosting.wh_ip_acl_whitelist.push(ip);
        return storeAcls(knex)

     }
     function removeAclIp(knex, ip){
        var index = webhosting.wh_ip_acl_whitelist.indexOf(ip);
        if(index < 0) return Promise.resolve()
        webhosting.wh_ip_acl_whitelist.splice(index, 1);
        return storeAcls(knex)
     }

     function storeAcls(knex){
       return knex("webhostings")
        .whereRaw("wh_webhosting=?", [webhostingId])
        .update({wh_ip_acl_whitelist: Common.ForceEnumerableToString(webhosting.wh_ip_acl_whitelist)})
     }
  }


  function megaBytesToBytes(mb) {
  	 return mb*1024*1024
  }

}
