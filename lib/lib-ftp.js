var lib = module.exports = function(app, knex){

  const KnexLib = require("MonsterKnex")
  const MError = require("MonsterError")
  var ValidatorsLib = require("MonsterValidators")
  const moment = require('MonsterMoment');

  const speakeasy = require("speakeasy");

  const passwordCommon = require("password-common.js")(app.config)
  var Common = ValidatorsLib.array()

  var vali = ValidatorsLib.ValidateJs()

  const webhostingsLib = require("lib-webhosting.js")
  var webhostings = webhostingsLib(app, knex)

  var re = {}
  function GetAccountsBy(filterHash){

    var f = KnexLib.filterHash(filterHash)

    return knex("ftpaccounts").select()
      .whereRaw(f.sqlFilterStr, f.sqlFilterValues)
      .then(rows=>{
          rows.map(row => dbRowToObject(row))
          Common.AddCleanupSupportForArray(rows)

          return Promise.resolve(rows)
      })

  }

  re.GetAccountsAndWebhosting = function(webhosting_id) {
    var are = {}
    return webhostings.insertOrUpdateWebhosting(webhosting_id)
      .then((awebhosting)=>{
        are.webhosting = awebhosting
        return re.GetAccountsForWebhosting(are.webhosting.wh_id)
      })
      .then(aaccounts=>{
        are.accounts = aaccounts
        are.current = are.accounts.length
        are.maximum = are.webhosting.template.t_ftp_max_number_of_accounts
        are.can_be_added = (are.current < are.maximum)

        return Promise.resolve(are)
      })

  }

  re.GetAccountsBy = GetAccountsBy
  re.GetAccount= function(webhostingId, username){
  	return GetAccountsBy({fa_webhosting: webhostingId, fa_username: username})
  	  .then(rows=>{
  	  	 Common.EnsureSingleRowReturned(rows, "ACCOUNT_NOT_FOUND")
  	  	 return Promise.resolve(rows[0])
  	  })
  }

  re.GetAccounts= function(){
  	return GetAccountsBy({})
  }
  re.GetAccountsForWebhosting= function(webhostingId){
  	return GetAccountsBy({fa_webhosting: webhostingId})
  }

  re.InsertBackup = function(in_data, trx, req){
     if(!trx) trx = knex;

     // at first checking whether the username already exists in a different webstore. That would be problematic
     return trx("ftpaccounts").select().whereRaw("fa_webhosting!=? AND fa_username=?", [in_data.fa_webhosting, in_data.fa_username])
       .then(rows=>{
          if(rows.length > 0) throw new MError("FTP_ACCCOUNT_ALREADY_BELONGS_TO_SOMEONE");


          var r = KnexLib.filterHash(in_data, true)
          return trx.raw("REPLACE INTO ftpaccounts ("+r.sqlFilterStr+") VALUES ("+r.sqlValuesStr+")", r.sqlFilterValues).return()
       })

   }

   vali.validators.rfc4716 = function(value, options, key, attributes){
      // allowing empty string to be able to remove the setting
      value = value.trim()
      if(value == "") return;

      value += "\n";

      if(value.length > 8192) return "ssh key has invalid size";
      if(!value.startsWith('---- BEGIN SSH2 PUBLIC KEY ----')) return "ssh key has no begin marker";
      if(!value.endsWith("---- END SSH2 PUBLIC KEY ----\n")) return "ssh key has no finish marker";
      attributes[key] = value;
   }

   vali.validators.nonMatchingPassword = function(value, options){
      if(options.in_data.fa_password == value)
          return "password can't be the same";
   }

  re.Insert = function(in_data, req, trx){
     if(!trx) trx = knex;
  	var d
  	var webhosting
 	return vali.async(in_data, {
 		fa_webhosting: {presence:true, isInteger:true},
        fa_password: ValidatorsLib.passwordEnforcements,
        fa_username: {presence: true, isString: true, nonMatchingPassword:{in_data: in_data}},
        fa_subdir: {isPath: {noPathCharacterRestrictions:true}, default: "/"},

 	})
 	.then(ad=>{
        d = ad

        if(d.fa_password.match(/^\*[0-9A-F]{40}$/)) {
           console.log("supplied password is mysql encoded, skipping pbkdf2")
           return Promise.resolve({password_hash:d.fa_password})
        }

        return passwordCommon.getPasswordHash(d.fa_password)
  })
  .then(pbkdf2 =>{
      	d.pw = pbkdf2 || {}

        return re.GetAccountsAndWebhosting(d.fa_webhosting)
 	})
 	.then((are)=>{
 		    webhosting = are.webhosting

   		if(!are.can_be_added)
 			   throw new MError("ACCOUNT_QUOTA_EXCEEDED")

 		// insert ftp account itself

       return trx
         .insert({
         	fa_webhosting: webhosting.wh_id,
          fa_user_id: webhosting.wh_user_id,
            fa_username: d.fa_username,
            fa_password: d.pw.password_hash,
            fa_pbkdf2_hash_algo: d.pw.pbkdf2_hash_algo || "-",
            fa_pbkdf2_output_length: d.pw.pbkdf2_output_length || 0,
            fa_pbkdf2_iterations: d.pw.pbkdf2_iterations || 0,
            fa_salt: d.pw.salt || "",
            fa_subdir: d.fa_subdir,
         })
         .into("ftpaccounts")
         .then(ids=>{

            app.InsertEvent(req, {
              e_user_id: webhosting.wh_user_id,
			  e_username: d.fa_username,
              e_event_type: "ftp-account-created",
              e_other: {subdir: d.fa_subdir}
            });

            return Promise.resolve(ids[0])
         })
         .catch(ex=>{
         	 console.error("error inserting account", ex)
         	 if(ex.code == "SQLITE_CONSTRAINT")
         	 	throw new MError("ALREADY_EXISTS")
         	 throw ex
         })

 	})

  }

  const report_tally_changes_interval_ms = (app.config.get("report_tally_changes_interval_sec")||0) * 1000
  if((report_tally_changes_interval_ms)&&(!app.report_tally_changes)) {
  	 app.report_tally_changes = true
  	 doReportTallyChanges()
  }


  return re

  function doReportTallyChanges(){
  	 setTimeout(function(){

	    return knex("tallies").select()
	      .whereRaw("t_tally_updated=1", [])
	      .then(rows=>{
             // schedule next round
             doReportTallyChanges()

	      	 if(rows.length <= 0) return Promise.resolve()

          return knex("tallies")
            .whereRaw("t_tally_updated=1", [])
            .update({t_tally_updated:0})
            .then(()=>{

                 var toSend = {}
                 rows.forEach(function(x){
                    toSend[x.t_webhosting] = {wh_tally_web_storage_mb: Math.round(x.t_tally_b/1024/1024 * 100) / 100}
                 })
                 console.log("reporting tally changes", toSend)

                 // this is started in the background, we are not particularly intersted in the result
                 app.GetWebhostingMapiPool().postAsync("/s/[this]/webhosting/tallies", toSend)


            })


	      })


  	 }, report_tally_changes_interval_ms)
  }




  function dbRowToObject(ftp) {
     ftp.UpdateLastLogin = function(ip){
          var now = moment.now()

          return knex("ftpaccounts")
            .whereRaw("fa_id=?", [ftp.fa_id])
            .update({fa_last_login_ip:ip,fa_last_login_ts:now})
            .then(()=>{
               return Promise.resolve()
            })
     }

  	 ftp.Cleanup = function(){
  	 	delete ftp.fa_id
      delete ftp.fa_password
  	 	return ftp
  	 }
  	 ftp.Delete = function(){
  	 	return knex("ftpaccounts")
  	 	  .whereRaw("fa_id=?", [ftp.fa_id])
  	 	  .del()
  	 	  .then(()=>{
  	 	  	 return Promise.resolve()
  	 	  })

  	 }

      ftp.ThrowIfInvalidToken = function(userToken){
          return vali.async({userToken:userToken}, {userToken: {presence: true, isString: {trim:true}}})
            .then(d=>{

              if(!ftp.fa_totp_secret){
                throw new MError("NOT_ACTIVATED");
              }

                var verified = speakeasy.totp.verify({ secret: ftp.fa_totp_secret,
                                                       encoding: 'base32',
                                                       token: d.userToken });

                if(!verified) throw new MError("INVALID_TOTP_TOKEN");
            })

          }
      ftp.RegenerateTotpToken = function(){
          var secret = speakeasy.generateSecret();
          ftp.fa_totp_secret = secret.base32;
          return knex("ftpaccounts").update({fa_totp_secret: ftp.fa_totp_secret}).whereRaw("fa_id=?",[ftp.fa_id]).return();
      }
      ftp.InactivateTotp = function(){
          return knex("ftpaccounts").update({fa_totp_active: false, fa_totp_secret: ""}).whereRaw("fa_id=?",[ftp.fa_id]).return();
      }
      ftp.ActivateTotp = function(){
          return knex("ftpaccounts").update({fa_totp_active: true}).whereRaw("fa_id=?",[ftp.fa_id]).return();
      }


      ftp.GetSshAuthz = function(){
         return Promise.resolve({
            authz: ftp.fa_ssh_authorized_key
         });
      }

      ftp.SetSshAuthz = function(in_data){
         return vali.async(in_data, {authz:{isString: true, rfc4716: true}})
           .then((d)=>{
               return knex("ftpaccounts").update({fa_ssh_authorized_key: d.authz}).whereRaw("fa_id=?",[ftp.fa_id]);
           })
      }


  	 return ftp
  }


  function megaBytesToBytes(mb) {
  	 return mb*1024*1024
  }

}
