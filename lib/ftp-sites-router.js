module.exports = function(app, knex) {

  const webhostingsLib = require("lib-webhosting.js")
  var webhostings = webhostingsLib(app, knex)

  var router = app.ExpressPromiseRouter()

  router.route("/admin-ips")
    .delete(function(req,res,next){
       return req.sendOk(
           webhostings.CleanupOldAdminIps()
       )

    })
    .post(function(req,res,next){
       return req.sendOk(
           webhostings.StoreAdminIps(req.body.json)
       )

    })

  router.route("/:webhosting")
    .get(function(req,res,next){
       return req.sendPromResultAsIs(
            getWebhosting(req)
            .then(wh=>{
              return wh.Cleanup()
            })
        )

    })
    .delete(function(req,res,next){
      // this is for cleanup, removing all accounts
	  var wh;
       return req.sendOk(
            getWebhosting(req)
            .then(awh=>{
			  wh = awh;
              return wh.Delete()
            })
             .then(()=>{
                app.InsertEvent(req, {
                  e_event_type: "ftp-site-deleted",
				  e_user_id: wh.wh_user_id,
                  e_other: {webhosting_id: req.params.webhosting}
                });
             })
        )
    })

  router.route("/:webhosting/acls")
    .put(function(req,res,next){
	     var wh;
         return req.sendOk(
            getWebhosting(req)
             .then(awh=>{
			     wh = awh;
                 return wh.AddAclIp(req.body.json)
             })
             .then(()=>{
                    app.InsertEvent(req, {
                      e_event_type: "ftp-acl-ip-added",
   				      e_user_id: wh.wh_user_id,
                      e_other: {ip: req.body.json.ip}
                    });
             })
         )
     })
    .delete(function(req,res,next){
	     var wh;
         return req.sendOk(
            getWebhosting(req)
             .then(awh=>{
			     wh = awh;
                 return wh.RemoveAclIp(req.body.json)
             })
             .then(()=>{
                    app.InsertEvent(req, {
                      e_event_type: "ftp-acl-ip-removed",
   				      e_user_id: wh.wh_user_id,
                      e_other: {ip: req.body.json.ip}
                    });
             })
         )
     })
    .post(function(req,res,next){
	     var wh;
         return req.sendOk(
            getWebhosting(req)
             .then(awh=>{
			     wh = awh;
                 return wh.SetAclIps(req.body.json)
             })
             .then(()=>{
                    app.InsertEvent(req, {
                      e_event_type: "ftp-acl-ips-changed",
   				      e_user_id: wh.wh_user_id,					  
                      e_other: req.body.json.reset
                    });
             })
         )
     })

  router.route("/:webhosting/public")
    .post(function(req,res,next){
	     var wh;
         return req.sendOk(
            getWebhosting(req)
             .then(awh=>{
			     wh = awh;
                 return wh.SetPublic(req.body.json)
             })
             .then(()=>{
                    app.InsertEvent(req, {
                      e_event_type: "ftp-site-public-changed",
   				      e_user_id: wh.wh_user_id,
                      e_other: {status: req.body.json.status}
                    });
             })
         )
     })



  router.route("/:webhosting/admin-login")
    .post(function(req,res,next){
	     var wh;
         return req.sendOk(
            getWebhosting(req)
             .then(awh=>{
			     wh = awh;
                 return wh.StoreAdminLogin(req.body.json)
             })
             .then(()=>{
                    app.InsertEvent(req, {
                      e_event_type: "ftp-site-admin-login-changed",
   				      e_user_id: wh.wh_user_id,
                      e_other: {status: req.body.json.status}
                    });
             })
         )

  })

  router.route("/tallies")
    .post(function(req,res,next){
         return req.sendOk(
           webhostings.DoAdjustTally(req.body.json)
         )
     })


  router.route("/")
    .get(function(req,res,next){
         return req.sendPromResultAsIs(
           webhostings.GetSites()
            .then(sites=>{
                return Promise.resolve(sites.Cleanup())
            })
         )
     })


  return router


  function getWebhosting(req) {
     return webhostings.GetWebhostingById(req.params.webhosting)
  }

}
