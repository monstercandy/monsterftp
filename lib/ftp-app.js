// permissions needed: ["ADMIN_TALLY","EMIT_LOG","INFO_WEBHOSTING"]
module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "ftp"

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')
    var fs = require("MonsterDotq").fs();

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');


    config.defaults({
      "last_admin_login_cleanup_interval_ms": 5*60*1000,
      "wh_last_admin_login_duration_sec": 86400,
      "report_tally_changes_interval_sec": 5*60,
      "commander_params": {
      },

      "cmd_ftpwho": {"executable":"/usr/local/sbin/dftpwho", "args": ["-f","/var/lib/proftpd/proftpd.scr","-o","json"]},
    })

	/*
   Array("mysql").forEach(k=>{
      if(!config.get(k)) throw new Error("Mandatory configuration option is missing: "+k)
    })
	*/

    config.appRestPrefix = "/ftp"

    var app = me.Express(config, moduleOptions);
    app.MError = me.Error

    var commanderOptions = app.config.get("commander") || {}
    app.commander = require("Commander")(commanderOptions)

    app.knex = require("MonsterKnex")(config)

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)

    var router = app.PromiseRouter(config.appRestPrefix)

    // external service providers will call this route
    router.RegisterPublicApis(["/tasks/"])
    router.RegisterDontParseRequestBody(["/tasks/"])

    const MonsterInfoLib = require("MonsterInfo")

    app.MonsterInfoWebhosting = MonsterInfoLib.webhosting({config: app.config, webhostingServerRouter: app.ServersRelayer})

    router.use("/tm", require("ftp-tm-router.js")(app, app.knex))

    router.use("/accounts", require("ftp-accounts-router.js")(app, app.knex))

    router.use("/sites", require("ftp-sites-router.js")(app, app.knex))

    router.use("/scoreboard", require("ftp-scoreboard-router.js")(app, app.knex))

    require("Wie")(app, router, require("lib-wie.js")(app, app.knex));

    app.GetWebhostingMapiPool = app.GetRelayerMapiPool;


    app.Prepare = function() {

       return app.knex.migrate.latest()
    }

    app.Cleanup = function() {

       var fn = config.get("db").connection.filename
       if(!fn) return Promise.resolve()

       return fs.unlinkAsync(fn).catch(x=>{}) // the database might not exists, which is not an error in this case

    }


    return app
}
