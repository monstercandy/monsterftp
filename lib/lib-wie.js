var lib = module.exports = function(app, knex) {

   const ftpLib = require("lib-ftp.js")
   var ftp = ftpLib(app, knex);

   const webhostingsLib = require("lib-webhosting.js")
   var webhostings = webhostingsLib(app, knex);

   var wie = {};

   const dotq = require("MonsterDotq");
   const moment = require("MonsterMoment");

   wie.BackupWh = function(wh, req) {
      var re = {};
      return ftp.GetAccountsForWebhosting(wh.wh_id)
        .then(accounts=>{
           re.accounts = accounts;

           accounts.forEach(row=>{
              delete row.fa_id;
              delete row.fa_user_id;
              delete row.fa_webhosting;
           })

           return webhostings.GetWebhostingByIdSimple(wh.wh_id)
             .catch(ex=>{
                if(ex.message == "WEBHOSTING_NOT_FOUND") return; // this is fine

                // otherwise throw
                throw ex;
             })
        })
        .then(site=>{
           if(site) {
             re.site = site;
             delete site.wh_user_id;
             delete site.wh_storage;
             re.tally = {
               t_tally_b: re.site.t_tally_b,
               t_tally_sum_b: re.site.t_tally_sum_b,
               t_tally_updated: re.site.t_tally_updated,
             };
             Object.keys(re.tally).forEach(x=>{
                delete re.site[x];
             });

             delete re.site.wh_last_admin_login_allowed_until;

             delete re.site.wh_webhosting;

           }

           return re;
        })
   }

   // wrapping the restore all operation in a huge transaction
   wie.RestoreAllWhs = function(whs, in_data, req){
      return knex.transaction(function(trx){
          var wieTrx = require("Wie").transformWie(app, lib(app, trx));
          return wieTrx.GenericRestoreAllWhs(whs, in_data, req);
      })
   }


   wie.GetAllWebhostingIds = function(){
      return webhostings.GetSites()
        .then(sites=>{
            // console.log("fooo!", sites)
            var re = [];
            sites.forEach(site=>{
               re.push(site.wh_webhosting);
            })
            return re;
        })
   }

   wie.RestoreWh = function(wh, in_data, req) {

      // lets put the site first
      return knex.transaction(function(trx){
          return Promise.resolve()
            .then(()=>{
               if(!in_data.site) return;
                in_data.site.wh_webhosting = wh.wh_id;
                in_data.site.wh_user_id = wh.wh_user_id;
                in_data.site.updated_at = moment.now();
                in_data.site.wh_storage = wh.extras.web_path;

                return webhostings.InsertSiteBackup(in_data.site, trx)
            })
            .then(()=>{
                if(!in_data.tally) return;

                in_data.tally.t_webhosting = wh.wh_id;
                return webhostings.InsertTallyBackup(in_data.tally, trx)
            })
            .then(()=>{

                return dotq.linearMap({array: in_data.accounts, action: function(account){

                    account.fa_webhosting = wh.wh_id;
                    account.fa_user_id = wh.wh_user_id;
                    account.updated_at = moment.now();

                    return ftp.InsertBackup(account, trx, req)
                }})

            })
            .then(()=>{
                app.InsertEvent(req, {e_event_type: "restore-wh-ftp", e_other: wh.wh_id})

            })

      })


   }



   return wie;

}
