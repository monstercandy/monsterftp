require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var password = process.argv[2]
var salt = process.argv[3]
if(!password) {
	console.error("Usage: node password-helper.js password [raw_salt_bytes]")
	process.reallyExit();
}


var config = require('MonsterConfig');
var passwordCommon = require("password-common.js")(config)

passwordCommon.getPasswordHash(password, salt)
  .then(h=>{
  	 console.log("(salt)", salt, "password",password, "pbkdf2", h)
  })
