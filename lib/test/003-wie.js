require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../ftp-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const storage_id = 32345

    var latestBackup;
    var getInfoCalls = 0;
            app.MonsterInfoWebhosting = {
               GetInfo: function(id){

                   getInfoCalls++

                   return Promise.resolve({
                     wh_id: id,
                     wh_user_id: 123,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }

    const expectedBackup = { accounts:
   [ {
       fa_username: 'wie-username',
       fa_subdir: '/subdir/',
       fa_totp_active: 0,
       fa_totp_secret: null,
       fa_ssh_authorized_key: null,
       fa_pbkdf2_hash_algo: 'sha256',
       fa_pbkdf2_output_length: 16,
       fa_pbkdf2_iterations: 1000,
       fa_last_login_ts: null,
       fa_last_login_ip: null,} ],
  site:
   { wh_quota_b: 10485760,
     wh_public_login: 1,
     wh_last_admin_login_ts: null,
     wh_last_admin_login_ip: null,
     wh_last_admin_login_enabled: 0,
     wh_ip_acl_effective: 0,
     wh_ip_acl_whitelist: [],
      },
  tally: {
     t_tally_b: 838860.8,
     t_tally_sum_b: 838860.8,
     t_tally_updated: 0 }
   };

      const userData1 = {
          fa_webhosting:storage_id,fa_username:"wie-username",fa_password:"wie-password",fa_subdir:"/subdir"
      }



	describe('basic tests', function() {

        it('inserting an ftp account with a subdir', function(done) {


             mapi.put( "/accounts", userData1, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })

        it('adjusting tallies', function(done) {

             mapi.post( "/sites/tallies", [
                {wh_id: storage_id, wh_tally_web_storage_mb: 0.8, wh_tally_sum_storage_mb: 0.8}
              ],
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                done()

             })

        })


	})


  describe("backup", function(){

        backupShouldWork();

  })



  describe("restore", function(){

      cleanup();

        it('get list of ftp accounts should be empty', function(done) {

             mapi.get( "/accounts/"+storage_id,function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })

        restoreShouldWork();

        // even twice, without clenaup!
        restoreShouldWork();

        backupShouldWork();

  });

  describe("site-wide backup", function(){

       it('generating backup for the complete site', function(done) {

             mapi.get( "/wie/", function(err, result, httpResponse){

                // console.log("foo", result)
                assert.ok(result[storage_id]);

                done()

             })

        })


       cleanup();

  })

  function restoreShouldWork(){
        it('restoring a specific webstore', function(done) {

             mapi.post( "/wie/"+storage_id, latestBackup, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })

        })
  }


  function cleanup(){
      it("cleanup first", function(done){
             mapi.delete( "/sites/"+storage_id, {}, function(err, result, httpResponse){

                assert.equal(result, "ok");
                done()

             })
      })
  }

  function backupShouldWork(){
       it('generating backup for a specific webstore', function(done) {

             mapi.get( "/wie/"+storage_id, function(err, result, httpResponse){

                latestBackup = simpleCloneObject(result);

                var actual = result;
                actual.accounts.forEach(row=>{
                   Array("fa_salt", "fa_password", "updated_at", "created_at").forEach(x=>{
                      assert.ok(row[x]);
                      delete row[x];
                   })

                   assert.notProperty(row, "fa_user_id");
                })
                Array("updated_at", "created_at").forEach(x=>{
                    assert.ok(actual.site[x]);
                    delete actual.site[x];
                })

                assert.notProperty(actual.site, "wh_user_id");

                assert.deepEqual(actual, expectedBackup);
                done()

             })

        })
  }


}, 10000)

