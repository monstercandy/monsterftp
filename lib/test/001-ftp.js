require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../ftp-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    const webhostingId1 = 12345
    const webhostingId2 = 12347

    const webhostingForAdminIpTest = 12346

    var getInfoCalls = 0
            app.MonsterInfoWebhosting = {
               GetInfo: function(id){
                   assert.ok(id >= webhostingId1 && id <= webhostingId2)

                   getInfoCalls++

                   return Promise.resolve({
                     wh_id: id, 
                     wh_user_id: 123,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }

      const userDataSame = {
          fa_webhosting:webhostingId1,fa_username:"username",fa_password:"username",fa_subdir:"/subdir"
      }
      const userData1 = {
          fa_webhosting:webhostingId1,fa_username:"username",fa_password:"password",fa_subdir:"/subdir"
      }
      const userData2 = {
          fa_webhosting:webhostingId1,fa_username:"username2",fa_password:"password",fa_subdir:"/subdir"
      }
      const userData3 = {
          fa_webhosting:webhostingId2,fa_username:"username",fa_password:"password",fa_subdir:"/subdir"
      }
      const userData4 = {
          fa_username:"username2",fa_password:"*22569C622455F7DD93A4DBF42554F07E320CD692",fa_subdir:"/subdir"
      }

      const userDataForAdminIpTest = {
          fa_webhosting:webhostingForAdminIpTest,fa_username:"usernameAdminIp",fa_password:"password"
      }


	describe('basic tests', function() {


        shouldBeEmpty()


        it('inserting an ftp account with same username and password should be rejected', function(done) {


             mapi.put( "/accounts", userDataSame, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "VALIDATION_ERROR")
                done()

             })

        })    


        it('inserting an ftp account with a subdir', function(done) {


             mapi.put( "/accounts", userData1, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })    


        it('second to the same webhosting should be rejected due to quota', function(done) {


             mapi.put( "/accounts", userData2, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "ACCOUNT_QUOTA_EXCEEDED")
                done()

             })

        })    

        it('username collision should be rejected', function(done) {


             mapi.put( "/accounts", userData3, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.propertyVal(err, "message", "ALREADY_EXISTS")
                done()

             })

        })    


        it('but otherwise put should still be ok (even with mysql encoded password!)', function(done) {


             mapi.put( "/accounts/"+webhostingId2, userData4, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })    


        it('query an account', function(done) {

             mapi.get( "/accounts/"+webhostingId1+"/"+userData1.fa_username, function(err, result, httpResponse){
                 // console.log("here",err, result)
                 assert.propertyVal(result, "fa_username", userData1.fa_username)
                 assert.propertyVal(result, "fa_subdir", "/subdir/")
                 assert.propertyVal(result, "fa_pbkdf2_hash_algo", "sha256")
                 assert.propertyVal(result, "fa_pbkdf2_output_length", 16)
                 assert.propertyVal(result, "fa_pbkdf2_iterations", 1000)
                 assert.property(result, "fa_salt")
                 assert.notProperty(result, "fa_password") // it should not be disclosed
                done()

             })

        })    

        it('query the account list via get', function(done) {

             mapi.get( "/accounts/"+webhostingId1, function(err, result, httpResponse){
                  // console.log("here",err, result)
                  assert.ok(Array.isArray(result))
                  assert.equal(result.length, 1)
                done()

             })

        })    

        it('query the account list via search for more info', function(done) {

             mapi.search( "/accounts/"+webhostingId1, function(err, result, httpResponse){
                  // console.log("here",err, result)
                  assert.property(result, "accounts")
                  assert.ok(Array.isArray(result.accounts))
                  assert.equal(result.accounts.length, 1)
                  assert.propertyVal(result, "current", 1)
                  assert.propertyVal(result, "maximum", 1)
                  assert.propertyVal(result, "can_be_added", false)
                done()

             })

        })    


        it('delete an account by name', function(done) {

             mapi.delete( "/accounts/"+webhostingId1+"/"+userData1.fa_username, {}, function(err, result, httpResponse){
                 assert.equal(result, "ok")
                done()

             })

        })    

        it('delete accounts of a webhosting', function(done) {
             var getInfoCallsBefore = getInfoCalls
             mapi.delete( "/sites/"+webhostingId2, {}, function(err, result, httpResponse){
                 assert.equal(getInfoCalls, getInfoCallsBefore+1, "GetInfo() on webhostingInfo should have been called")
                 assert.equal(result, "ok")
                done()

             })

        })    

        // and then everything should be empty again
        shouldBeEmpty()
	})


  describe("sites/tallies", function(){

        it('adjusting tallies', function(done) {

             mapi.post( "/sites/tallies", [
              {wh_id: webhostingId1, wh_tally_web_storage_mb: 0.8, wh_tally_sum_storage_mb: 0.8},
              {wh_id: webhostingId2, wh_tally_web_storage_mb: 1.3, wh_tally_sum_storage_mb: 1.3}
              ], 
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                done()

             })

        })    

        it('fetching sites, the tallies should be reflected', function(done) {

             mapi.get( "/sites/", 
              function(err, result, httpResponse){
                // console.log("yo", err, result)
                assert.ok(Array.isArray(result))
                result.forEach(row=>{
                  delete row.created_at
                  delete row.updated_at
                })
                 assert.deepEqual(result, [ { wh_webhosting: webhostingId1,
    wh_user_id: '123',
    wh_quota_b: 10485760,
    t_tally_b: 838860.8, // note: this is bytes here not mbyte
    t_tally_sum_b: 838860.8,
    t_tally_updated: 0,
    // wh_storage: "/web/w3/12345-sdfsdfsdf",
    wh_last_admin_login_ts: null,
    wh_last_admin_login_ip: null,
    wh_last_admin_login_enabled:0,
    wh_ip_acl_effective: 0,
    wh_ip_acl_whitelist: [], 
    wh_public_login:1 ,
  },
  { wh_webhosting: webhostingId2,
    wh_user_id: '123',
    wh_quota_b: 10485760,
    t_tally_b: 1363148.8,
    t_tally_sum_b: 1363148.8,
    t_tally_updated: 0,
    // wh_storage: "/web/w3/12346-sdfsdfsdf",
    wh_last_admin_login_ts: null,
    wh_last_admin_login_ip: null,
    wh_last_admin_login_enabled:0,
    wh_ip_acl_effective: 0,
    wh_ip_acl_whitelist: [],
    wh_public_login:1 ,
  } ])
                done()

             })

        })    

  })

  describe("acls", function(){

        it('changing acls', function(done) {

             mapi.post( "/sites/"+webhostingId1+"/acls", {reset:["1.1.1.1","2.2.2.2"]},
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                 done()
             })

        })    


        it('adding acl', function(done) {

             mapi.put( "/sites/"+webhostingId1+"/acls", {ip:"3.3.3.3"},
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                 done()
             })

        })    



        Array(
           {n:"ACL IP filter",q:"acls",d:"wh_ip_acl_effective"},
           {n:"admin login filter",q:"admin-login",d:"wh_last_admin_login_enabled"},
           {n:"public filter",q:"public",d:"wh_public_login"}
        ).forEach(c=>{

            it('enabling '+c.n, function(done) {

                 mapi.post( "/sites/"+webhostingId1+"/"+c.q, {status:true},
                  function(err, result, httpResponse){
                     assert.equal(result, "ok")
                     done()
                 })

            })    


            it('fetching site again, filtering should be on for '+c.n, function(done) {

                 mapi.get( "/sites/"+webhostingId1, 
                  function(err, result, httpResponse){
                    // console.log("yo", err, result)
                    assert.propertyVal(result, c.d, 1)
                    done()

                 })

            })   

            it('disable '+c.n, function(done) {

                 mapi.post( "/sites/"+webhostingId1+"/"+c.q, {status:false},
                  function(err, result, httpResponse){
                     assert.equal(result, "ok")
                     done()
                 })

            })    


            it('fetching site again, filtering should be off for '+c.n, function(done) {

                 mapi.get( "/sites/"+webhostingId1, 
                  function(err, result, httpResponse){
                    // console.log("yo", err, result)
                    assert.propertyVal(result, c.d, 0)
                    done()

                 })

            })   


        })



  })  


  describe("admin ips", function(){

        it('prechecking some sites, admin login ip should be empty', function(done) {

             mapi.get( "/sites/"+webhostingId1, 
              function(err, result, httpResponse){
                  // console.log("yo", err, result)

                  assert.notProperty(result, "wh_last_admin_login_allowed_until")
                  assert.propertyVal(result, "wh_last_admin_login_ts", null)
                  assert.propertyVal(result, "wh_last_admin_login_ip", null)
                  assert.propertyVal(result, "wh_last_admin_login_enabled", 0)
                  done()
             })

        })   


        it('storing admin login', function(done) {

             mapi.post( "/sites/admin-ips", {"123":"1.2.3.4"},
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                 done()
             })

        })    

        it('fetching site, the acls should be reflected', function(done) {

             mapi.get( "/sites/"+webhostingId1, 
              function(err, result, httpResponse){
                //  console.log("yo", err, result)

                  assert.property(result, "wh_last_admin_login_ts")
                  assert.isNotNull(result.wh_last_admin_login_ts)

                  assert.property(result, "wh_last_admin_login_allowed_until")

                  assert.propertyVal(result, "wh_last_admin_login_ip", "1.2.3.4")
                  assert.propertyVal(result, "wh_last_admin_login_enabled", 0)
                  done()
             })

        })   


        it('sending it once again', function(done) {

             mapi.post( "/sites/admin-ips", {"123":"4.3.2.1"},
              function(err, result, httpResponse){
                 assert.equal(result, "ok")
                 done()
             })

        })    

        compareSiteAdminIp('fetching site again, admin ip should have changed')

        it('inserting a new ftp account to a new webhosting should fetch the admin ip automatically', function(done) {


             mapi.put( "/accounts", userData1, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })    

        it('fetching site again, admin ip should have changed', function(done) {

             mapi.get( "/sites/"+webhostingForAdminIpTest, 
              function(err, result, httpResponse){
                //  console.log("yo", err, result)

                  assert.property(result, "wh_last_admin_login_ts")
                  assert.isNotNull(result.wh_last_admin_login_ts)

                  assert.property(result, "wh_last_admin_login_allowed_until")

                  assert.propertyVal(result, "wh_last_admin_login_ip", "4.3.2.1")
                  assert.propertyVal(result, "wh_last_admin_login_enabled", 0)
                  done()
             })

        })   

        it('cleaning up old admin ips', function(done) {

             mapi.delete( "/sites/admin-ips", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })  

        compareSiteAdminIp("admin ip should be still the same (was not old enough)")

        it('cleaning up old admin ips', function(done) {

             app.config.set("wh_last_admin_login_duration_sec", 0)

             mapi.delete( "/sites/admin-ips", {}, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })  

        it("admin ips should be cleared now", function(done) {

             mapi.get( "/sites/"+webhostingId1, 
              function(err, result, httpResponse){
                  //  console.log("yo", err, result)

                  assert.notProperty(result, "wh_last_admin_login_allowed_until")
                  assert.propertyVal(result, "wh_last_admin_login_ip", "")
                  done()
             })

        })  

        function compareSiteAdminIp(msg){
            it(msg, function(done) {

                 mapi.get( "/sites/"+webhostingId1, 
                  function(err, result, httpResponse){
                      //  console.log("yo", err, result)

                      assert.property(result, "wh_last_admin_login_ts")
                      assert.isNotNull(result.wh_last_admin_login_ts)

                      assert.property(result, "wh_last_admin_login_allowed_until")

                      assert.propertyVal(result, "wh_last_admin_login_ip", "4.3.2.1")
                      assert.propertyVal(result, "wh_last_admin_login_enabled", 0)
                      done()
                 })

            })             
        }

  })


// no means for testing this stuff as it would need external dependency - command execution
  describe('scoreboard', function() {

            const final_result = {
  "server": {
    "server_type": "standalone",
    "pid": 23187,
    "started_ms": 1489350751000
  },
  "connections": [
    {
      "pid": 4960,
      "connected_since_ms": 1489381151000,
      "remote_name": "112.85.42.123",
      "remote_address": "112.85.42.123",
      "local_address": "::ffff:88.151.102.104",
      "local_port": 22,
      "authenticating": true,
      "protocol": "ftp",
      "idling": true
    },
    {
      "pid": 24015,
      "connected_since_ms": 1489387932000,
      "remote_name": "178.48.246.208",
      "remote_address": "178.48.246.208",
      "local_address": "::ffff:88.151.102.104",
      "local_port": 21,
      "user": "ng.monstermedia.hu",
      "protocol": "ftp",
      "location": "/",
      "command": "STOR",
      "command_args": "proftpd-1.3.5d.tar.gz",
      "uploading": true,
      "transfer_duration_ms": 2285000
    }
  ]
}


       it('fetching scoreboard should return an empty array', function(done) {


            setupSpawner();

             mapi.get( "/scoreboard", function(err, result, httpResponse){
                 assert.deepEqual(result, final_result)
                 done()

             })

        })   


       it('fetching the time machine endpoint should return the same scoreboard', function(done) {


            setupSpawner();

             mapi.get( "/tm", function(err, result, httpResponse){
                 assert.deepEqual(result, {scoreboard:final_result})
                 done()

             })

        })   

       function setupSpawner(){
            app.commander = {
              spawn: function(cmd){
                  // console.log("spawn called", cmd)
                  assert.deepEqual(cmd, { chain: { executable: 'ftpwho', args: [ '-f', 'something' ] },
  removeImmediately: true,
  omitControlMessages: true,
  dontLog_stdout: true,
  executeImmediately: true })
                  return Promise.resolve({id:"123", executionPromise: Promise.resolve({output:JSON.stringify(final_result)})})
              }
            }
       }
  })


  function shouldBeEmpty(){    

        it('get global list of ftp accounts should be empty', function(done) {

             mapi.get( "/accounts",function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })    

        it('get list of ftp accounts for a webhosting should be empty', function(done) {

             mapi.get( "/accounts/"+webhostingId1,function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.deepEqual(result, [])
                done()

             })

        })    

  }


}, 10000)

