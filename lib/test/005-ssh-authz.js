require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../ftp-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var tokens = {}
    var userIds = {}

    var speakeasy = require("speakeasy");

    var userId = 124;

    var secret;

    var webhostingId1 = 10005;

      const userData1 = {
          fa_webhosting:webhostingId1,fa_username:"ssh-authz",fa_password:"password",fa_subdir:"/subdir"
      }

      const pubKey = `
---- BEGIN SSH2 PUBLIC KEY ----
Comment: "2048-bit RSA, converted by root@test from OpenSSH"
AAAAB3NzaC1yc2EAAAADAQABAAABAQDx6o3VCYX+aAYWPAxK7KUKBg9AkZGcfhBFWEvvX1
3YxoXqsRP9/wZcsC703cne+b2g1fi0+j9ArEW1AnOGO8A2lzsXOcsOVL5MJ3rwprEU/M4Q
LJjpksa19w10V5EzZhL1Q5whEjLd+YNeLkql2PFceFa634/kDxfejqBK3zUarcvAY8dXHN
CsvV/4Jz3cx6oZjQt7DRkoTxP3bMeA7reCc0Y136zxUzbasTOQ5LR2ZskvSnZtZ4wRdw45
S9QqHlxDxkYn4FFziNhCyVpuUPZoW7mouX5NXM36L9cQL19PYAGSH1TGmedocajpxOGkLC
Pk/uugHqk/Ek9Y/TmA5Vfl
---- END SSH2 PUBLIC KEY ----

                `;

            app.MonsterInfoWebhosting = {
               GetInfo: function(id){

                   return Promise.resolve({
                     wh_id: id, 
                     wh_user_id: userId,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }


    var email = "sshauthz@email.ee";

    const sshAuthzUrl = "/accounts/"+webhostingId1+"/"+userData1.fa_username+"/ssh-authz/";


    describe("prepare", function(){
        it('inserting an ftp account with a subdir', function(done) {


             mapi.put( "/accounts", userData1, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })    

    })


	describe('ssh authz tests', function() {

        it('getting the current setting', function(done) {

             mapi.get( sshAuthzUrl, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.deepEqual(result, {authz: null})

                done();

             })
        })

        it('setting a public key', function(done) {

             mapi.post( sshAuthzUrl, {authz: pubKey}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.equal(result, "ok")

                done();

             })
        })

        it('getting the current setting', function(done) {

             mapi.get( sshAuthzUrl, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.deepEqual(result, {authz: pubKey.trim()+"\n"})

                done();

             })
        })

        it('resetting the public key', function(done) {

             mapi.post( sshAuthzUrl, {authz: ""}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.equal(result, "ok")

                done();

             })
        })

        it('getting the current setting', function(done) {

             mapi.get( sshAuthzUrl, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.deepEqual(result, {authz: ""}) // note no new line: this is because of the extra proftpd where sql expression

                done();

             })
        })

    })





}, 10000)

