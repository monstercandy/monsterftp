require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../ftp-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var tokens = {}
    var userIds = {}

    var speakeasy = require("speakeasy");

    var userId = 123;

    var secret;

    var webhostingId1 = 10004;

      const userData1 = {
          fa_webhosting:webhostingId1,fa_username:"totp",fa_password:"password",fa_subdir:"/subdir"
      }

            app.MonsterInfoWebhosting = {
               GetInfo: function(id){

                   return Promise.resolve({
                     wh_id: id, 
                     wh_user_id: userId,
                     wh_storage: "/web/w3",
                     extras: {
                         web_path: "/web/w3/"+id+"-sdfsdfsdf"
                     },
                     template:{
                       t_ftp_max_number_of_accounts:1,
                       t_storage_max_quota_soft_mb:10,
                     },
                     wh_tally_web_storage_mb: 5.0,
                     wh_tally_sum_storage_mb: 5.2
                   })
               }
            }


    var email = "totp@email.ee";

    const totpBaseUrl = "/accounts/"+webhostingId1+"/"+userData1.fa_username+"/totp/";


    describe("prepare", function(){
        it('inserting an ftp account with a subdir', function(done) {


             mapi.put( "/accounts", userData1, function(err, result, httpResponse){
                // console.log("here",err, result)
                assert.equal(result, "ok")
                done()

             })

        })    

    })


	describe('totp tests', function() {


        it('grabbing a secret code', function(done) {

             mapi.post( totpBaseUrl+"status", {}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)

                assert.propertyVal(result, "fa_totp_active", 0);
                assert.property(result, "fa_totp_url");

                secret = result.fa_totp_secret;

                done();

             })
        })

        it('trying to deactivate totp', function(done) {


             mapi.post( totpBaseUrl+"inactivate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "NOT_ACTIVATED", done)

             })
        })

        it('trying to activate totp with invalid code', function(done) {


             mapi.post( totpBaseUrl+"activate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "INVALID_TOTP_TOKEN", done)

             })
        })

        it('activating totp with correct code', function(done) {

            var token = speakeasy.totp({
              secret: secret,
              encoding: 'base32'
            });

             mapi.post( totpBaseUrl+"activate", {"userToken": token}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.equal(result, "ok");

                done()  
             })
        })


        it('status page shall return already activate', function(done) {
             mapi.post( totpBaseUrl+"status", {}, function(err, result, httpResponse){
                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.deepEqual(result, {fa_totp_active: 1})

                done()  

             })
        })


        it('trying to activate totp again', function(done) {

             mapi.post( totpBaseUrl+"activate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "ALREADY_ACTIVATED", done)

             })
        })


        it('trying to deactivate totp with invalid code again', function(done) {


             mapi.post( totpBaseUrl+"inactivate", {"userToken": "123456"}, function(err, result, httpResponse){

                assert.app_error(err, result, httpResponse, "INVALID_TOTP_TOKEN", done)

             })
        })

        it('deactivating totp with correct code', function(done) {

              var token = speakeasy.totp({
                  secret: secret,
                  encoding: 'base32'
              });

             mapi.post( totpBaseUrl+"inactivate", {"userToken": token}, function(err, result, httpResponse){

                assert.equal(result, "ok");

                done();

             })
        })

    })





}, 10000)

