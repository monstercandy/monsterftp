require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../ftp-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var processLib = require("../log-processing.js");
    var process = processLib(app, app.knex)

	describe('basic tests', function() {

        Array("foobar", "2017-03-12 13:12:03,339 monstermedia proftpd[31724] 88.151.102.104 (178.48.246.208[178.48.246.208]): XXXX foglalas: Login successful.").forEach(line=>{

			it("processline should discard lines where nothing was recognized: "+line, function(){
				assert.isFalse(process.processLine(line));
			})
        })


        Array(
{
	expected:{
		pid: "24415",
		ip: '80.99.178.158',
		reason: "Login successful.",
		successful: true,
		username: "foglalas",
	},
	line:'2019-03-04 17:22:44,347 stan proftpd[24415] 212.47.236.1 (80.99.178.158[80.99.178.158]): USER foglalas: Login successful.',
},

        	{expected:{ pid: '31724',
  ip: '178.48.246.208',
  username: 'foglalas',
  reason: 'Login successful.',
  successful: true }, line:"2017-03-12 13:12:03,339 monstermedia proftpd[31724] 88.151.102.104 (178.48.246.208[178.48.246.208]): USER foglalas: Login successful."},
        	{expected: { pid: '31929',
  ip: '95.110.156.190',
  username: 'mobilhomokteam',
  reason: 'no such user found from 95.110.156.190 [95.110.156.190] to ::ffff:88.151.102.104:21',
  successful: false }, line:"2017-03-12 13:13:29,403 monstermedia proftpd[31929] 88.151.102.104 (95.110.156.190[95.110.156.190]): USER mobilhomokteam: no such user found from 95.110.156.190 [95.110.156.190] to ::ffff:88.151.102.104:21"},
            {expected: { pid: '32087',
  ip: '112.86.74.40',
  username: 'root',
  reason: 'no such user found from 112.86.74.40 [112.86.74.40] to ::ffff:88.151.102.104:22',
  successful: false }, line:"2017-03-12 13:14:29,394 monstermedia proftpd[32087] 88.151.102.104 (112.86.74.40[112.86.74.40]): USER root: no such user found from 112.86.74.40 [112.86.74.40] to ::ffff:88.151.102.104:22"}
        ).forEach(c=>{
			it("login events should be recognized: "+c.line, function(){
				var oprocessLoginEvent = process.processLoginEvent;
				var wasCalled = false;
				process.processLoginEvent = function(params){
					// console.log(params)
					assert.deepEqual(params, c.expected)
					wasCalled = true;
				}
				assert.isTrue(process.processLine(c.line));
				assert.isTrue(wasCalled);
				process.processLoginEvent = oprocessLoginEvent;
		    });

        });



			it("clamav events should be recognized", function(){
				var oprocessClamavEvent = process.processClamavEvent;
				var wasCalled = false;
				process.processClamavEvent = function(params){
					// console.log(params)
					assert.deepEqual(params, { pid: '12857',
  ip: '80.99.228.220',
  virus_name: 'Eicar-Test-Signature',
  webhosting_id: '16613',
  filename: 'aligatorfarm.hu/eicar.txt' })
					wasCalled = true;
				}
				assert.isTrue(process.processLine("2017-09-11 07:09:05,141 monstermedia proftpd[12857] 88.151.102.104 (80.99.228.220[80.99.228.220]): mod_clamav/0.14rc1: Virus 'Eicar-Test-Signature' found in '/web/w3/16613-313564750a37476db03cd06d418d25e4/aligatorfarm.hu/eicar.txt'"));
				assert.isTrue(wasCalled);
				process.processClamavEvent = oprocessClamavEvent;
		    });


    })


})

