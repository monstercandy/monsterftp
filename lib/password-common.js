var lib = module.exports = function(config) {


  return {
	  getPasswordHash: function(plainPassword, salt){
	  	  var salt
	  	  const TokenLib = require("Token")
        var configHash ={}
        Array("pbkdf2_iterations","pbkdf2_output_length", "pbkdf2_hash_algo", "salt_length").forEach(q=>{
            configHash[q] = config.get(q)
        })

        configHash.salt = salt

	  	  return TokenLib.GetPasswordHash(plainPassword, configHash)
	  }
  }

}  
