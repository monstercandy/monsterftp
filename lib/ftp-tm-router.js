module.exports = function(app, knex) {

    const slib = require("ftp-scoreboard-router.js");

    var router = app.ExpressPromiseRouter();

    router.get("/", function(req,res,next){
        return req.sendPromResultAsIs(
            slib.GetScoreboard(app)
              .then(sc=>{
                  return {scoreboard: sc};
              })
        )
    })


  return router


}
