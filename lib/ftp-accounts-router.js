module.exports = function(app, knex) {

  const ftpLib = require("lib-ftp.js")
  var ftp = ftpLib(app, knex)

  var router = app.ExpressPromiseRouter()

  const MError = app.MError;

  router.post("/tallies",function(req,res,next){
         return req.sendOk(
         	 ftp.DoAdjustTally(req.body.json)
         )
  })

  router.get("/:webhosting/:username/ssh-authz", function(req,res,next) {
       var account;
       return ftp.GetAccount(req.params.webhosting, req.params.username)
        .then((account)=>{
           return req.sendPromResultAsIs(account.GetSshAuthz());
        })
  });

  router.post("/:webhosting/:username/ssh-authz", function(req,res,next) {
       var account;
       return ftp.GetAccount(req.params.webhosting, req.params.username)
        .then((account)=>{
           return account.SetSshAuthz(req.body.json);
        })
        .then(()=>{
           return req.sendOk();
        })
  });

  router.post("/:webhosting/:username/totp/status", function(req,res,next) {
       var account;
       return ftp.GetAccount(req.params.webhosting, req.params.username)
        .then((aAccount)=>{
            account = aAccount;
            if(!account.fa_totp_secret) return account.RegenerateTotpToken();
        })
        .then(()=>{
            var d = {
              fa_totp_active: account.fa_totp_active,
            };
            if(!account.fa_totp_active) {
              var name = req.params.username.replace(" ", "");
              d.fa_totp_secret = account.fa_totp_secret;
              d.fa_totp_url = `otpauth://totp/${name}?secret=${account.fa_totp_secret}`;
            }

            return req.sendResponse(d);
        })
  });
  router.post("/:webhosting/:username/totp/activate", function(req,res,next) {
       var account;
       return ftp.GetAccount(req.params.webhosting, req.params.username)
        .then((aAccount)=>{
            account = aAccount;
            if(account.fa_totp_active) {
                throw new MError("ALREADY_ACTIVATED");
            }
            return account.ThrowIfInvalidToken(req.body.json.userToken, false)
        })
        .then(()=>{

            // all good.
            return account.ActivateTotp();          
        })
        .then(()=>{
           app.InsertEvent(req, {e_event_type: "totp-activate"})

           return req.sendOk();
        })

  });
  router.post("/:webhosting/:username/totp/inactivate", function(req,res,next) {
       var account;
       return ftp.GetAccount(req.params.webhosting, req.params.username)
        .then((aAccount)=>{
            account = aAccount;

            if(!account.fa_totp_active) {
              throw new MError("NOT_ACTIVATED");
            }

            return account.ThrowIfInvalidToken(req.body.json.userToken, true);
        })
        .then(()=>{

            // all good.
            return account.InactivateTotp();          
        })
        .then(()=>{
           app.InsertEvent(req, {e_event_type: "totp-deactivate"})

           return req.sendOk();
        })

  });

  router.route("/:webhosting/:username")
    .get(function(req,res,next){
       return ftp.GetAccount(req.params.webhosting, req.params.username)
           .then(a=>{
           	  return req.sendResponse(a.Cleanup())
           })
    })
    .delete(function(req,res,next){
	   var a;
       return req.sendOk(
         ftp.GetAccount(req.params.webhosting, req.params.username)
           .then(aa=>{
		      a = aa;
           	  return a.Delete()
           })
           .then(()=>{
              app.InsertEvent(req, {
			    e_user_id: a.fa_user_id,
			    e_username: a.fa_username,
                e_event_type: "ftp-account-removed",
                e_other: req.params
              });
           })
       )

    })

  router.route("/:webhosting")
    .put(function(req,res,next){
       req.body.json.fa_webhosting = req.params.webhosting
       return addAccount(req)
    })
    .get(function(req,res,next){
         return req.sendPromResultAsIs(ftp.GetAccountsForWebhosting(req.params.webhosting)
           .then(x=>{
         	 return Promise.resolve(x.Cleanup())
           })
         )
    })
    .search(function(req,res,next){

        var accounts
         return req.sendPromResultAsIs(ftp.GetAccountsAndWebhosting(req.params.webhosting)
            .then(are=>{
               var re = are
               delete re.webhosting
               return Promise.resolve(re)
            })

          )
    })

  router.route("/")
    .put(function(req,res,next){
       return addAccount(req)
    })
    .get(function(req,res,next){
  	// this should list all accounts
         return req.sendPromResultAsIs(ftp.GetAccounts().then(x=>{
         	 return Promise.resolve(x.Cleanup())
         }))
     })


  return router


  function addAccount(req) {
  	 return req.sendOk(
  	 	ftp.Insert(req.body.json, req)
  	 )
  }


}
