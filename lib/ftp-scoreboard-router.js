(function(){
  var lib = module.exports = function(app) {

      var router = app.ExpressPromiseRouter()

      router.get("/", function(req,res,next){

         return req.sendPromResultAsIs(
            lib.GetScoreboard(app)
         );
      })


    return router


  }

  lib.GetScoreboard = function(app){

         var cmd = app.config.get("cmd_ftpwho")
         if(!cmd) throw new MError("NOT_CONFIGURED")

         return app.commander.spawn(
            {chain: cmd, removeImmediately: true, omitControlMessages: true, dontLog_stdout: true, executeImmediately: true},
            app.config.get("commander_params")
         )
           .then(h=>{
              return h.executionPromise
           })
           .then(o =>{
              return JSON.parse(o.output.toString());
           })

  }

})();
